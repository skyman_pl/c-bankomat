#include <semaphore.h>

#ifndef _BUSINESSLOGIC_H_
#define _BUSINESSLOGIC_H_

typedef struct
{
    float Balance;
    sem_t Semaphore;
} BankAccount;

void childBusinessLogic(BankAccount * const sharedVar, const unsigned short childNum);

void initilizeSharedVariable(BankAccount * const sharedVar);

void cleanupSharedVariable(BankAccount * const sharedVar);

#endif // _BUSINESSLOGIC_H_
