#include <stdbool.h>
#include <businesslogic.h>

#ifndef _UTILITYFUNCTIONS_H
#define _UTILITYFUNCTIONS_H

void testIfErrorAndExit(bool condition, int errorCode, const char *msg);

pid_t forkNewChildRunAndExit(void (*childLogic)(BankAccount* const, const unsigned short childNum), BankAccount* const sharedVar, const unsigned short childNum);

void waitForChildProcesses(const pid_t childPidsArr[], const unsigned short childCount);

#endif // _UTILITYFUNCTIONS_H
