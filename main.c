#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <stdlib.h>

#include <config.h>
#include <utilityfunctions.h>
#include <businesslogic.h>

int main(void)
{
    const int shmId = shmget(IPC_PRIVATE,sizeof(BankAccount), IPC_CREAT | 0600);
    testIfErrorAndExit(shmId < 0, -1, "shmget");
    
    BankAccount * const shmVar = shmat(shmId, NULL, 0);
    testIfErrorAndExit(shmVar < 0, -2, "shmat");
    
    initilizeSharedVariable(shmVar);
    
    testIfErrorAndExit(sem_wait(&shmVar->Semaphore) < 0, -3, "sem_wait server");
    
    printf("$$ Current balance %.2f\n", shmVar->Balance);
    printf("$$ Server up and running\n");
    
    pid_t childPidArr[CHILD_PROCESS_NUMBER];
    unsigned short childCount = 0;
    
    for(childCount = 0; childCount < CHILD_PROCESS_NUMBER; ++childCount)
        childPidArr[childCount] = forkNewChildRunAndExit(childBusinessLogic, shmVar, childCount);
        
    printf("$$ Child processes running\n");
    
    testIfErrorAndExit(CHILD_PROCESS_NUMBER < childCount, -5, "CHILD_PROCESS_NUMBER");
    
    testIfErrorAndExit(sem_post(&shmVar->Semaphore) < 0, -6, "sem_post server");
    
    waitForChildProcesses(childPidArr, childCount);
    
    printf("$$ Work done, cleaning up\n");
    printf("$$ Current balance %.2f\n", shmVar->Balance);
    
    cleanupSharedVariable(shmVar);
    
    testIfErrorAndExit(shmdt(shmVar) < 0, -3, "shmdt");
    
    testIfErrorAndExit(shmctl(shmId, IPC_RMID, NULL) < 0, -4, "shmctl rmid");
    
    return 0;
}

void childBusinessLogic(BankAccount * const sharedVar, const unsigned short childNum)
{
    struct timespec waitTime = {1, 0};
    
    srand(childNum*7734);
    unsigned int maxOps = rand()%10;
    for(unsigned int i = 0; i < maxOps; ++i)
    {
        while(sem_timedwait(&sharedVar->Semaphore, &waitTime) < 0);
    
        float oldBalance = sharedVar->Balance;
        float opAmount = (rand()%(2*MAX_ABSOLUTE_INITIAL_BALANCE*100))/100.0f - MAX_ABSOLUTE_INITIAL_BALANCE;
        float newBalance = oldBalance + opAmount;
        
        if(newBalance < MAX_CREDIT)
            printf("Vending machine #%d operation exceeded credit, operation amount %.2f, current balance %.2f\n", childNum, opAmount, sharedVar->Balance);
        else
        {
            sharedVar->Balance = newBalance;
            printf("Vending machine #%d old balance %.2f, operation amount %.2f, current balance %.2f\n", childNum, oldBalance, opAmount, sharedVar->Balance);
        }

        testIfErrorAndExit(sem_post(&sharedVar->Semaphore) < 0, -11, "sem_post");
    }
}

void initilizeSharedVariable(BankAccount * const sharedVar)
{
    sharedVar->Balance=(rand()%(2*MAX_ABSOLUTE_INITIAL_BALANCE*100))/100.0f - MAX_ABSOLUTE_INITIAL_BALANCE;
    sem_init(&sharedVar->Semaphore, 0, 1);
}

void cleanupSharedVariable(BankAccount * const sharedVar)
{
    sem_destroy(&sharedVar->Semaphore);
}
