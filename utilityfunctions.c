#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
       
#include <utilityfunctions.h>

void testIfErrorAndExit(bool condition, int errorCode, const char *msg)
{
    if(condition)
    {
        perror(msg);
        exit(errorCode);
    }
}

pid_t forkNewChildRunAndExit(void (*childLogic)(BankAccount* const, const unsigned short childNum), BankAccount* const sharedVar, const unsigned short childNum)
{
    pid_t childPid = fork();
    testIfErrorAndExit(childPid < 0, -1, "fork process");
    
    if(childPid == 0)
    {
        (*childLogic)(sharedVar, childNum);
        exit(0);
    }
    // meanwhile in parent
    return childPid;
}

void waitForChildProcesses(const pid_t childPidsArr[], const unsigned short childCount)
{
    for(int i=0; i < childCount; ++i)
        waitpid(childPidsArr[i], NULL, 0);
}
